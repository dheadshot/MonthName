#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int noyear=0;
char *isodate = NULL;

int main(int argc, char *argv[])
{
    char months[12][30] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    
    if (argc < 2 || strcmp(argv[1],"--help")==0 || strcmp(argv[1],"/?")==0)
    {
        fprintf(stderr,"MonthName 0.01.00\nUsage:\n  %s [--no-year] <ISO Date>\n", argv[0]);
        return 1;
    }
    if (argc>2 && (strcmp(argv[1],"--no-year")==0 || strcmp(argv[1],"-Y")==0))
    {
        noyear=1;
        isodate=argv[2];
    }
    else
    {
        isodate=argv[1];
    }
#ifdef DEBUG
    printf("Date: %s\n",isodate);
#endif
    
    unsigned int month,year;
    if (sscanf(isodate,"%u-%u-%*u %*s",&year,&month) < 2)
    {
        fprintf(stderr,"Error: Failed to read a valid ISO date from \"%s\"\n",isodate);
        return 2;
    }
    if (month>12)
    {
        fprintf(stderr,"Error: Invalid month \"%u\"\n",month);
        return 2;
    }
    month--;
#ifdef DEBUG
    printf("Date: [%u] %u\n",month,year);
#endif
    if (noyear)  printf("%s\n", months[month]);
    else printf("%s %u\n", months[month],year);
    return 0;
}
