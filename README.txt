# MonthName

C program that extracts the Month Name and Year (unless specified) from a given ISO date.


## Usage:
```
  monthname [--no-year] <ISO Date>
```

The option `--no-year` stops MonthName printing the year.
